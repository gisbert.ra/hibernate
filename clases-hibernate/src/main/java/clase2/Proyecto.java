package clase2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity
public class Proyecto {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private Date fechaFin;
	
	@ManyToMany
	private List <Empleado> empleados = new ArrayList <Empleado>();
	
	public Proyecto() {		
	}

	public Proyecto(String nombre, Date fechaFin) {
		this.nombre = nombre;
		this.fechaFin = fechaFin;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public void agregarEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
	}
	
	public void quitarEmpleado(Empleado empleado) {
		this.empleados.remove(empleado);
	}
}
