package clase2;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Cliente {

	@Id
	@GeneratedValue
	private Long id;
	private String cuit;
	private String nombre;
	
	@OneToMany
	private List <Proyecto> proyectos = new ArrayList<Proyecto>();
	
	public Cliente(String cuit, String nombre) {
		this.cuit = cuit;
		this.nombre = nombre;
	}
	
	public Cliente() {
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void agregarProyecto(Proyecto proyecto) {
		this.proyectos.add(proyecto);
	}
	
	public void quitarProyecto(Proyecto proyecto) {
		this.proyectos.remove(proyecto);
	}
}
