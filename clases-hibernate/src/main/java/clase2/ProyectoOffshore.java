package clase2;

import javax.persistence.Entity;

@Entity
public class ProyectoOffshore extends Proyecto {
// SIN ID!!
	private String pais;
	private String frontOffice;	
	
	public ProyectoOffshore () {		
	}

	public ProyectoOffshore(String pais, String frontOffice) {
		this.pais = pais;
		this.frontOffice = frontOffice;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getFrontOffice() {
		return frontOffice;
	}

	public void setFrontOffice(String frontOffice) {
		this.frontOffice = frontOffice;
	}
}
