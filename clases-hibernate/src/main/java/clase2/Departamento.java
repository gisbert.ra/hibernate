package clase2;
	
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departamento {
	@Id
	@GeneratedValue
	private Long id;
	private String codigo;
	private String nombre;

	@OneToMany(mappedBy = "departamento", cascade = CascadeType.ALL)
	private List <Empleado> empleados = new ArrayList<Empleado>();
		
	public Departamento() {		
	}

	public Departamento(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}
	
	public Long getId() {
		return id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void agregarEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
		empleado.setDepartamento(this);
	}
	
	public void quitarEmpleado(Empleado empleado) {
		this.empleados.remove(empleado);
		empleado.setDepartamento(null);
	}
}
