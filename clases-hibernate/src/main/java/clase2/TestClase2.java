package clase2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestClase2 {
	
	private static EntityManagerFactory emf;	
	
	@BeforeClass
	public static void setUp() throws Exception{
		emf = Persistence.createEntityManagerFactory("clases-hibernate");
	}

	@AfterClass
	public static void tearDown() throws Exception{
		emf.close();
	}	

	@Test
	public void testCrear() {	
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Empleado employee = new Empleado(1, "Ricardo");
		Empleado employee2 = new Empleado(2, "Gonzalo");
		Empleado employee3 = new Empleado(3, "Adrian");
		Empleado employee4 = new Empleado(4, "Carlos");
		
		Departamento dptment = new Departamento("Finanzas", "Contaduria");
		dptment.agregarEmpleado(employee);
		dptment.agregarEmpleado(employee2);
		dptment.agregarEmpleado(employee3);
		dptment.agregarEmpleado(employee4);
		Departamento dptment2 = new Departamento("Finanzas", "Tesoreria");
		
		UnidadDeNegocio und = new UnidadDeNegocio("Codigo_de_la_UND", "Nombre_de_la_UND");
		und.agregarDepartamento(dptment);
		
		Proyecto proy = new ProyectoLocal("CBA", 10);
		proy.agregarEmpleado(employee);
		
		Cliente cliente = new Cliente("33302032033", "Repsol");
		cliente.agregarProyecto(proy);
		
		em.persist(dptment);
		em.persist(dptment2);
		em.persist(und);
		em.persist(proy);
		em.persist(cliente);
		em.getTransaction().commit();
		em.close();		
	}
	
	@Test
	public void testLeer() {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();		
		List <Empleado> result = em.createQuery("from Empleado", Empleado.class).getResultList();
		for(Empleado empleado : result) {
			System.out.println("Nombre: "+empleado.getNombre()+" Depto: "+empleado.getDepartamento());
		}
		em.getTransaction().commit();
		em.close();		
	}	
}
