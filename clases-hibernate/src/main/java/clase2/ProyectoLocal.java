package clase2;

import javax.persistence.Entity;

@Entity
public class ProyectoLocal extends Proyecto {
	// SIN ID!!
	private String provincia;
	private int cantRecursosEnCliente;	

	public ProyectoLocal() {		
	}

	public ProyectoLocal(String provincia, int cantRecursosEnCliente) {
		this.provincia = provincia;
		this.cantRecursosEnCliente = cantRecursosEnCliente;
	}

	public String getprovincia() {
		return provincia;
	}

	public void setprovincia(String provincia) {
		this.provincia = provincia;
	}

	public int getCantRecursosEnCliente() {
		return cantRecursosEnCliente;
	}

	public void setcantRecursosEnCliente(int cantRecursosEnCliente) {
		this.cantRecursosEnCliente = cantRecursosEnCliente;
	}
}
