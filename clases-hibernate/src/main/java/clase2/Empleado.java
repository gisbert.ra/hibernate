package clase2;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Empleado {
	@Id
	@GeneratedValue
	private Long id;
	private int legajo;
	private String nombre;
	
	@ManyToOne
	private Departamento departamento;
	
	@ManyToMany(mappedBy = "empleados")
	private List <Proyecto> proyectos = new ArrayList <Proyecto>();
	
	public Empleado() {
	}

	public Empleado(int legajo, String nombre) {
		this.legajo = legajo;
		this.nombre = nombre;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void agregarProyecto(Proyecto proyecto) {
		this.proyectos.add(proyecto);
	}
	
	public void quitarProyecto(Proyecto proyecto) {
		this.proyectos.remove(proyecto);
	}
}
