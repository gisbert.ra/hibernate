package clase2;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UnidadDeNegocio {

	@Id
	@GeneratedValue
	private Long id;
	private String codigo;
	private String nombre;	
	@OneToMany
	private List <Departamento> departamento = new ArrayList<>();
	
	public UnidadDeNegocio (){		
	}
	
	public UnidadDeNegocio(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void agregarDepartamento(Departamento dpto) {
		this.departamento.add(dpto);
	}
}
