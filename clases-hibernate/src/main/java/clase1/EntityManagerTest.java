package clase1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class EntityManagerTest {
	private static EntityManagerFactory emf;	
	
	@BeforeClass
	public static void setUp() throws Exception{
		emf = Persistence.createEntityManagerFactory("clases-hibernate");
	}

	@AfterClass
	public static void tearDown() throws Exception{
		emf.close();
	}	

	@Test
	public void testCrear() {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		ClienteClase1 c = new ClienteClase1(2003581, "Cliente 2");
		c.setDir(new Direccion("Domicilio2"));
		
		em.persist(c);
		em.getTransaction().commit();
		em.close();		
	}
	
	@Test
	public void testLeer() {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();		
		List <ClienteClase1> result = em.createQuery("from Cliente", ClienteClase1.class).getResultList();
		for(ClienteClase1 cliente : result) {
			System.out.println(cliente.getRazonSocial()+" "+cliente.getDir().getDomicilio());
		}
		em.getTransaction().commit();
		em.close();		
	}	
}
